.. include:: replace.txt

ns-3 AppStore User WorkFlow
---------------------------

This section explains the User workflows on the AppStore website. This section will be useful to regular |ns3| users or developers or maintainers who interact with the website.

What are ns-3 apps?
*******************
'Apps' is just a colloquial term for what |ns3| calls a module or a group of modules. The |ns3| app store is a browsable front-end tool for users to find modules of interest. The end result is for users to download, configure, and build the set of modules of interest.

In most scenarios (outside of |ns3|), downloadable 'apps' are executable binaries or runtime plugins, and the act of downloading an app makes it immediately available for use on the user's system. |ns3|, however, is distributed as source code, and the user is responsible for compiling the code. This extends also to |ns3| apps (modules); the act of downloading does not directly result in a runnable extension. Instead, installing an app means that the user has the source code available to combine with other modules, ready to build.

Terminology
***********

1. An |ns3| module is a collection of models built as a shared library and linked with |ns3| main programs.
2. An |ns3| fork is a modified version of the |ns3| mainline release or development code.

Walkthrough
***********

The main page lists recently published apps, app categories, and provides a search bar

.. image:: ../figures/appstore/main-page.png

|ns3| Users are the primary target audience of this App Store. Users will be able to search for different Apps/Modules available in |ns3|, visit the individual App pages and download them.

Search the AppStore
===================

Users will be able to search for an App from the search bar located in the navbar of the website.

.. image:: ../figures/appstore/search-icon.png

Users can search for Apps based on Category, Author, Title, Abstract, and Description.
The search results can be sorted based on Downloads, Release Date, Name and Votes.

.. image:: ../figures/appstore/search-sort.png

App Resources
=============
The resources menu on the right hand side of the app page tell us:
    * Whether the app is a module extension or a fork
    * Download button may link to a source archive or a release page.  The latest version of the module is displayed with its release date and the version of ns-3 with which it is compatible
    * The links for users to follow to get more information or help with this module or fork

.. image:: ../figures/appstore/resources-menu.png

Users can download the App from the App Page which displays the following:
    * Details: Provides a summary and citations related to the module.
    * Release History: lists all releases of the module, the compatible ns-3 version, and a Bakefilemodulespecif available
    * Installation: Lists out any specialized installation steps or requirements
    * Maintenance: A user can get information on who is maintaining the module, and where to get some support on usage or problems encountered
    * Development: Describes on how to develop further enhancements, and where to send modifications (Patches, Pull Requests)

.. image:: ../figures/appstore/app-left.png